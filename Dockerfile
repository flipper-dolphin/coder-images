FROM alpine:edge
USER root

RUN echo '@community https://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories


# Installs shell related tools
RUN apk --no-cache add sudo tini shadow bash \
# Installs compatibility libs
  gcompat libc6-compat libgcc libstdc++ \
# Installs some basic tools
  git curl socat openssh-client nano unzip brotli zstd xz gnupg \
# Kube
  kubectl helm opentofu@community k9s \
# Docker
  docker zsh go neovim \
  ripgrep fd py-pip make \
# ssh extension no work otherwise
  procps

RUN curl -s https://fluxcd.io/install.sh | sudo bash



ARG USERNAME=coder
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Add group and user
RUN addgroup $USERNAME -g $USER_GID && \
    adduser -G $USERNAME -u $USER_UID -s /bin/bash -D $USERNAME && \
    echo $USERNAME ALL=\(ALL\) NOPASSWD:ALL > /etc/sudoers.d/nopasswd

# Change user
USER $USERNAME
RUN bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh) --no-install-dpeendencies; \
  pip install pynvim --break-system-packages; \
  echo "export PATH=~/.local/bin:$PATH" > /home/coder/.zshenv 

# Configure a nice terminal
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
# Fake poweroff (stops the container from the inside by sending SIGHUP to PID 1)
RUN echo "alias poweroff='kill -1 1'" >> /home/$USERNAME/.bashrc
RUN chsh -s $(which zsh)
WORKDIR /home/$USERNAME
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/bin/zsh"]
